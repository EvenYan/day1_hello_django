from django.apps import AppConfig


class ClasstestConfig(AppConfig):
    name = 'classtest'

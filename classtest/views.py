from django.shortcuts import render
from django.http import HttpResponse
from classtest.models import StudentInfo

# Create your views here.

# def index(request):
#     return HttpResponse("Hello, Django")

def home(request):
    return render(request, "classtest/index.html")

def index(request):
    students = StudentInfo.objects.all()
    return render(request, "index.html", {"students":students})

def index_page(request, id):
    student = StudentInfo.objects.get(pk=id)
    return render(request, "detail.html", {"student":student})

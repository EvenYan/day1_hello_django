from django.db import models

# Create your models here.

class ClassInfo(models.Model):
    ctitle = models.CharField(max_length=20)
    cpub_date = models.DateTimeField()

    def __str__(self):
        return "%d"%self.pk


class StudentInfo(models.Model):
    sname = models.CharField(max_length=20)
    sgender = models.BooleanField()
    scontent = models.CharField(max_length=100)
    sclass = models.ForeignKey("ClassInfo")
    def __str__(self):
        return "%d"%self.pk
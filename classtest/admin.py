from django.contrib import admin
from classtest.models import ClassInfo
from classtest.models import StudentInfo


def gender(self):
    if self.sgender:
        return "男"
    else:
        return "女"
gender.short_description = '性别'

class StudentInfoAdmin(admin.ModelAdmin):
    list_display = ['id', 'sname', gender, 'scontent']

# Register your models here.
admin.site.register(ClassInfo)
admin.site.register(StudentInfo, admin_class=StudentInfoAdmin)

import xadmin
xadmin.site.register(ClassInfo)
xadmin.site.register(StudentInfo)